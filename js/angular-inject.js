
(function (window, angular, undefined) {
    'use strict';

    injectDirective.$inject = ['$injector'];
    function injectDirective($injector) {
        return {
            restrict: 'A',
            scope: true,
            controller: '@',
            priority: 500
//            compile: function injectCompile($element, $attr) {
//                var expression = $attr.inject;
//                var match = expression.match(/^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?\s*$/);
//                var services = match[1];
//                var aliasAs = match[2];
//
//                services = services.substr(0, services.length-1);
//                services = services.substr(1).split(', ');
//                console.log(services);
//
//                if (!match) {
//                    throw Error("Expected expression in form of '_array_' or '_array_ as _alias_'.");
//                }
//
//                if (aliasAs && (!/^[$a-zA-Z_][$a-zA-Z0-9_]*$/.test(aliasAs) ||
//                    /^(null|undefined|this|\$index|\$parent)$/.test(aliasAs))) {
//                    throw Error("alias '" + aliasAs + "' is invalid --- must be a valid JS identifier which is not a reserved name.");
//                }
//
//                return {
//                    pre: function injectPreLink($scope, $element, $attr, ctrl, $transclude) {
//                        angular.forEach(services, function injectPreAction(serviceName) {
//                            console.log(serviceName);
//                            var obj = $injector.get(serviceName);
//                            if (aliasAs) {
//                                $scope[aliasAs][serviceName] = obj;
//                            } else {
//                                $scope[serviceName] = obj;
//                            }
//                        });
//                    }
//                }
//            }
        }
    }

    angular
        .module('angular-inject', [])
        .directive('inject', injectDirective);
})(window, window.angular);