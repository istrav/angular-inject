
angular
    .module('services', [])
    .service('employee', function () {
        this.name = 'employee';
        this.pay = angular.noop;
    })
    .service('user', function () {
        this.name = 'user';
        this.serve = angular.noop;
    })
    .service('customer', function () {
        this.name = 'customer';
        this.help = angular.noop;
    });